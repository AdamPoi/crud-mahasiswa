<?php

namespace App\Livewire\Pages\Admin\Mahasiswa;

use App\Livewire\Forms\MahasiswaForm;
use App\Models\MahasiswaModel;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithFileUploads;

class MahasiswaModal extends Component
{
    use WithFileUploads;


    public $photo;
    public MahasiswaForm $form;

    public function save()
    {
        $saved = $this->form->save();
        if ($saved) {
            $this->dispatch('mahasiswa-added');
        } else {
            $this->dispatch('mahasiswa-updated');
        }
    }

    #[On('delete')]
    public function delete($id)
    {
        $mahasiswa = MahasiswaModel::find($id);
        if ($mahasiswa->delete()) {
            $this->dispatch('mahasiswa-deleted');
        }
    }

    #[On('edit')]
    public function edit($id)
    {
        $this->form->setMahasiswa(MahasiswaModel::find($id));
        $this->dispatch('mahasiswa-edit');
    }

    #[On('reset')]
    public function resetForm()
    {
        $this->form->reset();
        $this->form->resetValidation();
    }

    #[On('updatedPhoto')]
    public function updatedPhoto()
    {
        $this->form->foto_profil = $this->photo;
    }
    public function render()
    {
        return view(
            'livewire.pages.admin.mahasiswa.mahasiswa-modal',
        );
    }
}
