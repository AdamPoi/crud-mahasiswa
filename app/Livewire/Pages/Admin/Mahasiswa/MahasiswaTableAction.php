<?php

namespace App\Livewire\Pages\Admin\Mahasiswa;

use Livewire\Component;

class MahasiswaTableAction extends Component
{
    public $mahasiswa;

    public function mount($mahasiswa)
    {
        $this->mahasiswa = $mahasiswa;
    }

    public function render()
    {
        return view('livewire.pages.admin.mahasiswa.mahasiswa-table-action');
    }
}
