<?php

namespace App\Livewire\Pages;

use App\Models\MahasiswaModel;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Mahasiswa extends Component
{
    use WithPagination;

    public $id;

    public $nama_lengkap;

    public $nim;

    public $jenis_kelamin;

    public $tanggal_lahir;

    public $tempat_lahir;

    public $no_telepon;

    public $email;

    public $alamat_lengkap;

    public $foto_profil;

    public function render()
    {
        return view('livewire.pages.mahasiswa', [
            'mahasiswa' => MahasiswaModel::paginate(2),

        ]);
    }

    public function store()
    {
        $mahasiswa = MahasiswaModel::create([
            'id' => Str::orderedUuid(),
            'nama_lengkap' => $this->nama_lengkap,
            'nim' => $this->nim,
            'email' => $this->email,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'tempat_lahir' => $this->tempat_lahir,
            'no_telepon' => $this->no_telepon,
            'alamat_lengkap' => $this->alamat_lengkap,
            'foto_profil' => $this->foto_profil,
        ]);

        $this->dispatch('menu-added');
        back();
    }

    public function delete($id)
    {
        MahasiswaModel::find($id)->delete();
        back();
    }

    public function show($id)
    {
        $this->id = $id;
        $mahasiswa = MahasiswaModel::find($id);
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->nim = $mahasiswa->nim;
        $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
        $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
        $this->tempat_lahir = $mahasiswa->tempat_lahir;
        $this->no_telepon = $mahasiswa->no_telepon;
        $this->email = $mahasiswa->email;
        $this->alamat_lengkap = $mahasiswa->alamat_lengkap;
        $this->foto_profil = $mahasiswa->foto_profil;
        $this->dispatch('show-menu');
    }

    public function update()
    {
        $id = $this->id;
        $mahasiswa = MahasiswaModel::find($id);
        $mahasiswa->update([
            'nama_lengkap' => $this->nama_lengkap,
            'nim' => $this->nim,
            'email' => $this->email,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'tempat_lahir' => $this->tempat_lahir,
            'no_telepon' => $this->no_telepon,
            'alamat_lengkap' => $this->alamat_lengkap,
            'foto_profil' => $this->foto_profil,
        ]);
        $this->dispatch('menu-updated');
        back();
    }
}
