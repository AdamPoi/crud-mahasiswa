<?php

namespace App\Livewire\Forms;

use App\Models\MahasiswaModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule as ValidationRule;
use Livewire\Attributes\On;
use Livewire\Attributes\Validate;
use Livewire\Form;
use Livewire\WithFileUploads;

class MahasiswaForm extends Form
{
    use WithFileUploads;

    public ?MahasiswaModel $mahasiswa;

    public string $nama_lengkap = '';

    public string $nim = '';

    public string $jenis_kelamin = 'L';

    public string $tempat_lahir = '';

    public string $tanggal_lahir = '';

    public string $email = '';

    public string $nomor_telepon = '';

    public string $alamat_lengkap = '';

    // #[Validate('nullable|image|max:5120|mimes:jpg,jpeg,png')]
    public $foto_profil = null;
    // protected $id;

    public function rules(): array
    {
        return [
            'nama_lengkap' => 'required|max:255',
            'nim' => [
                'required',
                'digits_between:10,15',
                isset($this->mahasiswa) ? ValidationRule::unique('mahasiswas')->ignore($this->mahasiswa->id) : 'unique:mahasiswas,nim',
            ],
            'jenis_kelamin' => 'required|in:L,P',
            'tempat_lahir' => 'required|max:80',
            'tanggal_lahir' => 'required|date',
            'email' => [
                'required',
                'email',
                'max:120',
                isset($this->mahasiswa) ? ValidationRule::unique('mahasiswas')->ignore($this->mahasiswa->id) : 'unique:mahasiswas,email',
            ],
            'nomor_telepon' => 'required|digits_between:10,15',
            'alamat_lengkap' => 'required',
            'foto_profil' => [
                'nullable',
                empty($this->mahasiswa) ? 'image' : '',
                empty($this->mahasiswa) ? 'mimes:jpg,jpeg,png' : '',
                'max:5120',
            ],
        ];
    }

    public function setMahasiswa(MahasiswaModel $mahasiswa)
    {
        $this->mahasiswa = $mahasiswa;
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->nim = $mahasiswa->nim;
        $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
        $this->tempat_lahir = $mahasiswa->tempat_lahir;
        $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
        $this->email = $mahasiswa->email;
        $this->nomor_telepon = $mahasiswa->nomor_telepon;
        $this->alamat_lengkap = $mahasiswa->alamat_lengkap;
        $this->foto_profil = $mahasiswa->foto_profil;
    }

    public function save()
    {
        $validated = $this->validate();

        if (empty($this->mahasiswa)) {
            $validated['id'] = str::uuid();
            $validated['foto_profil'] = $this->saveImage();
            $newMahasiswa = MahasiswaModel::create($validated);

            return $newMahasiswa;
        } else {
            $oldFotoProfil = $this->mahasiswa->foto_profil;
            $validated['foto_profil'] = $this->saveImage($oldFotoProfil);

            $this->mahasiswa->update($validated);
        }
        $this->reset();
    }

    public function saveImage($oldPhoto = null)
    {
        if ($this->foto_profil == $oldPhoto) {

            return $oldPhoto;
        }
        if (isset($this->foto_profil) && !empty($oldPhoto)) {
            Storage::disk('public')->delete('mahasiswa/foto_profil/' . $oldPhoto);
        }

        $fileName = Str::uuid() . '.' . $this->foto_profil->extension();
        $this->foto_profil->storeAs('mahasiswa/foto_profil/', $fileName, 'public');

        return $fileName;
    }
}
