<?php

namespace App\Livewire\Components\Atoms;

use Illuminate\Support\Str;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class FileUpload extends Component
{
    use WithFileUploads;

    #[Rule('nullable|image|max:5120')]
    public $files = [];

    public function render()
    {
        return view('livewire.components.atoms.file-upload');
    }

    #[On('file-upload')]
    public function handleFileUpload($path)
    {
        $fileNames = [];
        // upload file to storage
        foreach ($this->files as $file) {
            $fileName = Str::uuid().'.'.$file->extension();
            $file->storeAs($path, $fileName, 'public');
            array_push($fileNames, $fileName);
        }
        // save file to database
        $this->dispatch('file-uploaded', $fileNames);
    }
}
