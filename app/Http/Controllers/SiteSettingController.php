<?php

namespace App\Http\Controllers;

use App\DataTables\SiteSettingDataTable;

class SiteSettingController extends Controller
{
    public function index(SiteSettingDataTable $datatable)
    {
        return $datatable->render('pages.admin.site-setting.index');
    }
}
