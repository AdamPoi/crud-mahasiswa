<?php

namespace App\Http\Controllers\User;

use App\DataTables\User\GenerateTokenTable;
use App\Http\Controllers\Controller;

class GenerateTokenController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-generate-token');
    }

    public function index(GenerateTokenTable $dataTable)
    {
        return $dataTable->render('pages.admin.users.token.index');
    }
}
