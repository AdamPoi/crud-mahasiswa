<?php

namespace App\Http\Controllers\User;

use App\DataTables\User\UserDataTable;
use App\Http\Controllers\Controller;

class UserListController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admin-mahasiswa');
    }

    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render('pages.admin.users.userlist.index');
    }
}
