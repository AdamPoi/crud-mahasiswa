<?php

namespace App\DataTables;

use App\Models\MahasiswaModel;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Livewire\Livewire;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MahasiswaDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param  QueryBuilder  $query  Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'mahasiswa.action')
            ->addIndexColumn()
            ->addColumn('action', function (MahasiswaModel $val) {
                return Livewire::mount('pages.admin.mahasiswa.mahasiswa-table-action', ['mahasiswa' => $val]);
            })
            ->editColumn('foto_profil', function (MahasiswaModel $val) {
                if ($val->foto_profil) {
                    return "<img src='".asset('storage/mahasiswa/foto_profil/'.$val->foto_profil)."' class='rounded-circle' width='80' height='80' style='object-fit:cover;object-position:center;' alt='foto profil'>";
                } else {
                    return "<img src='".asset('assets/media/avatars/blank.png')."' class='img-fluid rounded-circle' width='80' height='80' style='object-fit:cover;object-position:center;'alt='foto profil'>";
                }
            })
            ->editColumn('jenis_kelamin', function (MahasiswaModel $val) {
                return $val->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan';
            })

            ->rawColumns(['action', 'foto_profil'])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(MahasiswaModel $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('mahasiswa-table')
            ->columns($this->getColumns())
            ->minifiedAjax(script: "
                data._token = '".csrf_token()."';
                data._p = 'POST';
            ")
            ->dom('rt'."<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>")
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->orderBy(2)
            ->drawCallbackWithLivewire(file_get_contents(public_path('assets/js/custom/table/_init.js')))
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('foto_profil')->title('Profil'),
            Column::make('nim')->title('NIM')->searchable(true),
            Column::make('nama_lengkap')->searchable(true)->title('Nama Lengkap')->orderable(true),
            Column::make('jenis_kelamin')->searchable(true)->title('Jenis Kelamin'),
            Column::make('tempat_lahir')->searchable(true)->title('Tempat Lahir'),

        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Mahasiswa_'.date('YmdHis');
    }
}
