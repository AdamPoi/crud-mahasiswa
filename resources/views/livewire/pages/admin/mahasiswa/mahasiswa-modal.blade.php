<div>
    <x-mollecules.modal id="form-mahasiswa-modal" action="save" enctype="multipart/form-data" wire:ignore.self>
        @if (isset($form->mahasiswa))
            <x-slot:title>Update Mahasiswa</x-slot:title>
        @else
            <x-slot:title>Add Mahasiswa</x-slot:title>
        @endif
        <div class="mb-6">
            <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
            <x-atoms.input name="nama_lengkap" wire:model='form.nama_lengkap' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>NIM</x-atoms.form-label>
            <x-atoms.input name="nim" wire:model='form.nim' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
            <select wire:model="form.jenis_kelamin" class="form-select" data-control="select2" data-hide-search="true"
                data-placeholder="Select Role" name="target_assign">
                <option value="L">Laki-Laki</option>
                <option value="P">Perempuan</option>
            </select>
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
            <x-atoms.input name="tempat_lahir" wire:model='form.tempat_lahir' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
            <x-atoms.input name="tanggal_lahir" type="date" wire:model='form.tanggal_lahir' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Email</x-atoms.form-label>
            <x-atoms.input name="email" wire:model='form.email' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>No Telepon</x-atoms.form-label>
            <x-atoms.input name="nomor_telepon" wire:model='form.nomor_telepon' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
            <x-atoms.textarea name="alamat_lengkap" wire:model='form.alamat_lengkap' />
        </div>
        <div class="mb-6">
            <x-atoms.form-label>Foto Profil</x-atoms.form-label>
            <x-atoms.upload-photo wire:model="photo" id="foto-profil-upload" wire:change="$dispatch('updatedPhoto')" />
        </div>
        @if ($form->foto_profil)
            <img id="preview-image"
                src="{{ $photo ? $photo->temporaryUrl() : asset('storage/mahasiswa/foto_profil/' . $form->foto_profil) }}"
                class='rounded-circle' width='80' height='80' style="object-fit:cover;object-position:center;"
                alt="foto profil">
        @endif
        <x-slot:footer>
            <button class="btn-primary btn" type="submit" wire:loading.attr="disabled"
                wire:target="foto_profil">Submit</button>
        </x-slot:footer>
    </x-mollecules.modal>
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:initialized', () => {

            $('#form-mahasiswa-modal').on('hidden.bs.modal', function(e) {
                $('#foto-profil-upload').val('');
                $(this).find('form').trigger('reset');
            });


            function refreshTable() {
                window.LaravelDataTables['mahasiswa-table'].ajax.reload();
            };
            @this.on('mahasiswa-added', () => {
                Swal.fire({
                    title: "Good job!",
                    text: "Success Menambahkan Data Mahasiswa!",
                    icon: "success"
                });
                $('#form-mahasiswa-modal').modal('hide');
                refreshTable();
            })
            @this.on('mahasiswa-updated', () => {
                Swal.fire({
                    title: "Good job!",
                    text: "Success Mengubah Data Mahasiswa!",
                    icon: "success"
                });
                $('#form-mahasiswa-modal').modal('hide');
                refreshTable();
            })

            @this.on('mahasiswa-edit', () => {
                $('#form-mahasiswa-modal').modal('show');
                refreshTable();
            })
            @this.on('mahasiswa-deleted', () => {
                refreshTable();
            })

        })
    </script>
@endpush
