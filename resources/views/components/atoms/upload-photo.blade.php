   @props(['name' => $attributes->get('wire:model')])

   <input type='file' accept='image/*' class="form-control" name="{{ $name }}" {{ $attributes }}
       @error($name) {{ $attributes->merge(['class' => 'is-invalid']) }}@enderror>
   <div wire:loading wire:target="{{ $name }}">Uploading...</div>

   @error($name)
       <small class="text-danger"> {{ $message }} </small>
   @enderror
