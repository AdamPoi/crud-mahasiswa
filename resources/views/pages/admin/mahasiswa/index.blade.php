<x-layouts.app>
    <x-slot:title>Mahasiswa</x-slot:title>
    <livewire:pages.admin.mahasiswa.mahasiswa-modal />

    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                    <input type="text" data-table-id="mahasiswa-table" data-action-search
                        class="form-control form-control-solid w-250px ps-13" placeholder="Search User"
                        id="mahasiswa-table-search" />
                </div>
                <div class="d-flex ms-2">
                    <x-atoms.select name="filter_by" id="filter-by-select">
                        <option value="2">NIM</option>
                        <option value="3">Nama Lengkap</option>
                        <option value="4">Jenis Kelamin</option>
                        <option value="5">Tempat Lahir</option>
                    </x-atoms.select>
                    {{-- <x-atoms.select name="filter_by" id="filter-by_field" wire:model='filter_by' :lists="$columnNames"/> --}}
                </div>
            </div>

            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <!--begin::Add user-->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#form-mahasiswa-modal">
                        <i class="ki-duotone ki-plus fs-2"></i>
                        <span>Add Mahasiswa</span>
                    </button>
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>
    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
    @endpush

    @push('scripts')
        <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        {{ $dataTable->scripts() }}
        <script>
            $(document).ready(() => {
                let keyword;
                let searchByIndex;

                $('[data-action-search]').on('keyup', function() {
                    clearTimeout($.data(this, 'timer'));
                    $(this).data('timer', setTimeout(searchBy, 500));
                })
                $('#filter-by-select').on('change', function() {
                    searchBy()
                })

                function searchBy() {
                    const table = $('#mahasiswa-table').DataTable()
                    keyword = $('[data-action-search]').val()
                    searchByIndex = $('#filter-by-select').val()
                    if (keyword == '') {
                        table.columns().search('').draw();
                    }
                    table.columns(searchByIndex).search(keyword).draw();
                }
            });
        </script>
    @endpush

</x-layouts.app>
