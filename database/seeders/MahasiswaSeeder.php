<?php

namespace Database\Seeders;

use App\Models\MahasiswaModel;
use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MahasiswaModel::factory()->count(10)->create();
    }
}
